import { CommonComponent } from '../common/common.component';
export declare class SelectComponent extends CommonComponent {
    enumNames(index: any): any;
    emptyOption(): string;
}
