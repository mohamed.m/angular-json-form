import { CommonComponent } from '../common/common.component';
export declare class MultiselectComponent extends CommonComponent {
    enumNames(index: any): any;
    emptyOption(): string;
}
