import { FormGroup } from '@angular/forms';
import { Schema } from './schema';
export declare class SchemaFormGroup extends FormGroup {
    schema: Schema;
    style: any;
}
