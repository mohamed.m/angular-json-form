import { FormArray } from '@angular/forms';
import { Schema } from './schema';
export declare class SchemaFormArray extends FormArray {
    schema: Schema;
    style: any;
}
